import java.util.Random;

public class Board{
	private Tile[][] grid;
	private int size;
	private Random rng;
	
	public Board(int size) {
		this.size = size;
		this.rng = new Random();
		grid = new Tile[size][size];
		for (int i = 0; i<grid.length; i++){
			int randIndex = rng.nextInt(size);
			for (int j = 0; j<grid[i].length; j++) {
				grid[i][j] = Tile.BLANK;
			}
			grid[i][randIndex] = Tile.HIDDEN_WALL;
		}
	}
	
	//makes board
	public String toString(){
		String output = "";
		for (int i = 0; i < this.grid.length; i++) {
			for (int j = 0; j < this.grid.length; j++) {
				output += this.grid[i][j].getName() + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	//check position of where want to place
	public int placeToken(int row, int col) {
		if (row >= this.size || row <0 || col >= this.size || col < 0) {
			return -2;
		}
		if (grid[row][col].equals(Tile.CASTLE) || grid[row][col].equals(Tile.WALL)) {
			return -1;
		}
		if (grid[row][col].equals(Tile.HIDDEN_WALL)){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		grid[row][col] = Tile.CASTLE;
		return 0;
		
	}
	
}