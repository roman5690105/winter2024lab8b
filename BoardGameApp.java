import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Welcome to the castle something something game!");
		
		int boardSize = 5;
		Board gameBoard = new Board(boardSize);
		
		int numCastles = 7;
		int turns = 0; 
		
		//game loop
		while(numCastles>0 && turns < 8){
			System.out.println(gameBoard);
			System.out.println(numCastles);
			System.out.println(turns);
			
			System.out.println("input row:");
			int row = Integer.parseInt(reader.nextLine());
		
			System.out.println("input column:");
			int col = Integer.parseInt(reader.nextLine());
			
			int tokenReturn = gameBoard.placeToken(row, col);
			
			//check valid input or not
			//loops until valid input
			while(tokenReturn < 0){
				System.out.println("");
				System.out.println("invalid input");
				System.out.println("please input row:");
				row = Integer.parseInt(reader.nextLine());
		
				System.out.println("please input column:");
				col = Integer.parseInt(reader.nextLine());
			
				tokenReturn = gameBoard.placeToken(row, col);
			}
			
			if(tokenReturn == 1){
				System.out.println("there was a wall there");
				turns ++;
			}
			if(tokenReturn == 0){
				System.out.println("castle placed successfully");
				turns ++;
				numCastles --;
			}
			System.out.println("");
		}
		
		//check if game won
		System.out.println(gameBoard);
		if(numCastles == 0){
			System.out.println("you won!");
		}
		else{
			System.out.println("you lost!");
		}
	}
	
	
}